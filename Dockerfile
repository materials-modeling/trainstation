# Base image
FROM python:3.7

# Base packages
RUN \
  apt-get update -qy && \
  apt-get upgrade -qy

# Set up Python3 packages via pip
# 1. Base packages
# 2. Packages for testing
# 3. Packages needed for grapple
# 4. Packages for setting up documentation
RUN \
  pip3 install --upgrade \
    pip \
    setuptools \
    twine \
    && \
  pip3 install --upgrade \
    coverage \
    flake8 \
    mypy \
    pytest \
    xdoctest \
    && \
  pip3 install --upgrade \
    numpy \
    scikit-learn \
    scipy \
    && \
  pip3 install --upgrade \
    sphinx_autodoc_typehints \
    sphinx-rtd-theme \
    sphinx_sitemap \
    sphinxcontrib-bibtex \
    cloud_sptheme

CMD /bin/bash
