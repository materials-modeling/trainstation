.. _bibliography:
.. index:: Bibliography

Bibliography
************

.. [AngMunRah19]
   | M. Ångqvist, W. A. Muñoz, J. M. Rahm, E. Fransson, C. Durniak, P. Rozyczko, T. H. Rod, and P. Erhart
   | *ICET – A Python Library for Constructing and Sampling Alloy Cluster Expansions*
   | Adv. Theory. Sim. **2**, 1900015 (2019)
   | `doi: 10.1002/adts.201900015 <https://doi.org/10.1002/adts.201900015>`_

.. [EriFraErh19]
   | Fredrik Eriksson, Erik Fransson, and Paul Erhart
   | *The Hiphive Package for the Extraction of High-Order Force Constants by Machine Learning*
   | Advanced Theory and Simulations **2**, 1800184 (2019)
   | `doi: 10.1002/adts.201800184 <https://doi.org/10.1002/adts.201800184>`_

.. [FraEriErh20]
   | Erik Fransson, Fredrik Eriksson, and Paul Erhart
   | *Efficient construction of linear models in materials modeling and applications to force constant expansions*
   | npj Computational Materials **6**, 135 (2020)
   | `doi: 10.1038/s41524-020-00404-5 <https://doi.org/10.1038/s41524-020-00404-5>`_

.. [GolOsh09]
   | T. Goldstein and S. Osher
   | *The Split Bregman Method for L1-Regularized Problems*
   | SIAM Journal of Imaging Science **2**, 323 (2009)
   | `doi: 10.1137/080725891 <http://doi.org/10.1137/080725891>`_

.. [PedVarGra11]
   | F. Pedregosa *et al.*,
   | *Scikit-Learn: Machine Learning in Python*,
   | Journal of Machine Learning Research **12**, 2825 (2011)

.. [MueCed09]
   | T. Mueller, and G. Ceder
   | *Bayesian approach to cluster expansions*
   | Physical Review B **80**, 024103 (2009)
   | `doi: 10.1103/PhysRevB.80.024103 <https://doi.org/10.1103/PhysRevB.80.024103>`_

.. [ZhoSadAbe19]
   | F. Zhou, B. Sadigh, D. Åberg, Y. Xia, and V. Ozolins
   | *Compressive sensing lattice dynamics. II. Efficient phonon calculations and long-range interactions*
   | Physical Review B **100**, 184309 (2019)
   | `doi: 10.1103/PhysRevB.100.184309 <https://doi.org/10.1103/PhysRevB.100.184309>`_
